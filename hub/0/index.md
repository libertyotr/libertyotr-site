% Hub Index



<!-- **Enter [Hub 1](/hub/1)** ([alt](https://hubs.mozilla.com/ovaJ5mu/libertyotr)) -->
<!-- **Enter [Hub 1](/hub/1)** -->

 <a href="https://hubs.mozilla.com/4XMWcq6/libertyotr-1" class="btn btn-success" role="button">Join Now</a>


<!--[Enter Tab (long url)](https://hubs.mozilla.com/ovaJ5mu/this-coordinated-barbecue/) -->

<!--
### Hub #2

**Enter [Hub 2](/hub/2)** ([alt](https://hubs.mozilla.com/mgJxbAG/libertyotr-2/))
-->

---

### What are Hubs?
Hubs are experimental, VR-friendly rooms for communicating and collaborating privately.

## Troubleshooting

We try to use software that's fun for everyone, but it's a balance. If more then 10 people join, you're going to want a proper computer (not a mobile device).

#### Supported Browsers

Hubs requires specific features that aren't supported by all browsers. The following browsers are OK:

| Platform | Browsers
| --------------- | ----------- |
| Windows | Firefox, Chrome, Edge (version 79+) |
| Mac OS | Firefox, Chrome, Safari |
| Linux | Firefox |
| Android | Firefox, Chrome |
| iOS | Safari |


<!--[Enter Tab (long url)](https://hubs.mozilla.com/ovaJ5mu/this-coordinated-barbecue/) -->

<!--
### Hub #2

**Enter [Hub 2](/hub/2)** ([alt](https://hubs.mozilla.com/mgJxbAG/libertyotr-2/))
-->

---

### What are Hubs?
Hubs are experimental, VR-friendly rooms for communicating and collaborating privately.

## Troubleshooting

We try to use software that's fun for everyone, but it's a balance. If more then 10 people join, you're going to want a proper computer (not a mobile device).

#### Supported Browsers

Hubs requires specific features that aren't supported by all browsers. The following browsers are OK:

| Platform | Browsers
| --------------- | ----------- |
| Windows | Firefox, Chrome, Edge (version 79+) |
| Mac OS | Firefox, Chrome, Safari |
| Linux | Firefox |
| Android | Firefox, Chrome |
| iOS | Safari |
| Standalone VR | Oculus Browser, Firefox Reality |

#### Additional Resources

For more information, please consult the [Hubs by Mozilla Docs](https://hubs.mozilla.com/docs/hubs-troubleshooting.html).
