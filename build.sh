#!/usr/bin/env bash
##
##
## Copyright [2019], [Ampling]

set -e

version=0.0.1

arg1=${1:-}
arg2=${2:-}
mename=${0##*/}
usage="$mename $version [help]"



_lock() {
  exec 200>/tmp/.$(basename "$0").lock
    flock -n 200 \
        && return 0 \
        || return 1
}

_finish () {
  rm /tmp/.$(basename "$0").lock
  exit 0
}

run (){ 



pdsite build
rm ./public/build.sh
rmdir ./public/public 
find . -name index.html -print0 | 
xargs -0 sed -i '/>Public/d' ./public/index.html
# find . index.html -print0 | xargs -0 sed -i '/>Public/d' ./public/index.html
# sed -i '/>Public/d' ./public/index.html


}

[[ $arg1 == -h || $arg1 == *help* ]] && { printf '%s\n' "$usage" && exit 0 ;}

trap _finish EXIT
main() {
  _lock || { printf '%s\n' "in use" && exit 1 ;}
  run
}

main
