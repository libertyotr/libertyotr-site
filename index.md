% Liberty on the Rocks

***Philadelphia***

<img src="holidaybanner.jpg" alt="Holiday Meetup.jpg" style="height:200px;"> 

<!---
***This is a subtitle***
**Author:** *Philadelphia*
-->

---

A social evening for liberty enthusiasts who enjoy interesting discussions over cold drinks.

Our regular social evenings are not to be missed by liberty enthusiasts looking to meet other individuals, build their knowledge and friendships over warm food, cold drinks and good conversation.

Join us between 6–9 pm on hubs to chat over drinks. Occasionally, we host activities, games or invite a guest speaker to get the conversation started. It's pretty much a place for you to speak your mind. We welcome old friends and newcomers alike. If you're interested in the ideas of liberty and freedom this could be the event for you!

This is a rain or shine event, free and open to any individual, scheduled for every second and fourth Tuesday of the month.


<!-- We're a local network of libertarian thinkers who want more than an online conversation.  We aim to gather liberty-loving individuals of all political parties in the pubs, bars and restaurants of Philadelphia to keep the conversation going.  Meetings are suitable for drinkers and non-drinkers alike.  Come enjoy great discussions with others who endeavor to limit the reach of government into their personal lives and further the cause of liberty. -->

It's FREE to join our group

<br>

## Events

We aim for two events each month.

<br>

### 2nd Tuesday

-This second Tuesday we meet upstairs at Ryan's Pub in Manayunk. This is a social occasion for old and new friends to get together over cold drinks and discuss the issues.
This second Tuesday we meet: online

<!-- * 6-9pm, <a href="https://hubs.mozilla.com/4XMWcq6/libertyotr-1" class="btn btn-success" role="button">Join Now</a> -->

* 6-9:00pm, Ryan's Pub Manayunk, 4301 Main St, Philadelphia, PA 19127

### 4th Tuesday

This fourth Tuesday we meet on the lower level at the Adobe Cafe, in East Passyunk Philadelphia.  This is a social occasion for old and new friends to get together over cold drinks and discuss the issues.

* 6-9:00pm,  The Adobe Cafe, 736 S 8th St, Philadelphia, PA 19147

<br>
<br>

## Remote

Virtual rocks? Virtual rocks! We are now hosting online hubs to connect our liberty folk as per popular request.
See our liberty [hub index](/hub/0).

<a href="https://hubs.mozilla.com/4XMWcq6/libertyotr-1" class="btn btn-success" role="button">Join Now</a>

<br>
<br>


---

Links:

[libertyotr.eventbrite.com](https://libertyotr.eventbrite.com)

[Google Calendar](https://calendar.google.com/calendar?cid=cGhpbGFkZWxwaGlhQGxpYmVydHlvbnRoZXJvY2tzLm9yZw)

[libertyontherocks.org](https://libertyontherocks.org)

---

